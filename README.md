![Preview](chama.gif)

## Instalation instructions

1. Open **GENYMOTION**
2. Install all dependencies:

```bash
yarn
yarn start
```

3. In another terminal, run on android, for example:

```bash
yarn android
```

```javascript
  // const temp = text => {
  //   let reg = /@\[([^\]]+?)\]\(id:([^\]]+?)\)/gim;
  //   let textWithUserLink = this.formatTextWithMentions(text);
  //   let result = '';
  //   while ((result = reg.exec(textWithUserLink)) !== null) {
  //     const userId = result[2];
  //     textWithUserLink = result.replace(
  //       result[0],
  //       'http://localhost:3000/account/network/79659710-6e88-11ea-95fd-fbcc7249f24e'
  //     );
  //   }
  //   this.props.onChange(textWithUserLink);
  // };
```