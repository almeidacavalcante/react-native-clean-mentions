import styled from 'styled-components/native';

export const Container = styled.View``;

export const MentionTouchable = styled.TouchableOpacity`
  padding: 10px;
  margin: 0 0 1px 0;
  border-radius: 4px;
  background: #fff;
  display: flex;
  flex-direction: row;
`;
export const Profile = styled.Image`
  border-radius: 25px;
  width: 50px;
  height: 50px;
`;
