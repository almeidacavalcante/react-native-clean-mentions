import React from 'react';
import PropTypes from 'prop-types';

import { Text } from 'react-native';
import { Container, MentionTouchable, Profile } from './styles';
import defaultImage from '../../assets/default.png';

export default function Mention({ user, onPress }) {
  const handlePress = () => {
    onPress(user);
  };
  return (
    <Container>
      <MentionTouchable onPress={handlePress}>
        <Profile source={defaultImage} />
        <Text>{user.displayName}</Text>
      </MentionTouchable>
    </Container>
  );
}

Mention.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.string,
    username: PropTypes.string,
    displayName: PropTypes.string,
  }).isRequired,
  onPress: PropTypes.func.isRequired,
};
