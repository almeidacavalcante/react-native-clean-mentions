import React, { useState } from 'react';

import PropTypes from 'prop-types';
import { Container, TextArea, Hightlight } from './styles';
import MentionList from '../MentionList';

import util from '../../utils/stringManipulations';

export default function MentionEditor({
  value,
  onChange,
  users,
  pivotKey = 'username',
}) {
  const [isVisible, setIsVisible] = useState(false);
  const [lastSelection, setLastSelection] = useState();
  const [lastOnChangedText, setLastOnChangedText] = useState('');
  const [rawMentions, setRawMentions] = useState([]);

  const spaceRegex = /(\s)/g;

  const toggleMentionList = () => {
    setIsVisible(!isVisible);
  };
  const handleChange = text => {
    onChange(text);
    setLastOnChangedText(text);
  };

  const translateTextIntoMentions = text => {
    let translatedArray;
    if (text) {
      translatedArray = text.split(spaceRegex).map(item => {
        const mentionFound = rawMentions.find(mention => {
          return mention.displayName === item;
        });

        if (mentionFound) {
          return (
            <Hightlight key={mentionFound.displayName} tag={mentionFound.raw}>
              {mentionFound.displayName}
            </Hightlight>
          );
        }
        return item;
      });
    }
    const includes = translatedArray?.filter(item => {
      const found = rawMentions?.find(
        m => m.displayName === item?.props?.children
      );
      if (found) return true;
      return false;
    });

    const updated = includes?.map(inc => {
      return { raw: inc.props.tag, displayName: inc.props.children };
    });

    if (updated) {
      setRawMentions([...updated]);
    }
    return translatedArray;
  };

  function lastTypedChar(selection) {
    const { start, end } = selection;
    const lastTyped =
      start === end
        ? lastOnChangedText[start - 1]
        : lastOnChangedText[lastOnChangedText.length - 1];
    return lastTyped;
  }

  const handleSelectionChange = event => {
    const { selection } = event.nativeEvent;
    setLastSelection(selection);

    lastTypedChar(selection) === '@' ? setIsVisible(true) : setIsVisible(false);

    const translated = translateTextIntoMentions(lastOnChangedText);
    onChange(translated);
  };

  const handleSelectedUser = user => {
    toggleMentionList();
    const { start: position } = lastSelection;
    const mentionPosition = rawMentions.length;
    const raw = `[${user.username}](id:${user.id})`;
    const displayName = `${user.username}#${mentionPosition}`;

    setRawMentions([...rawMentions, { raw, displayName }]);

    const result = `${util.insertTextAt(
      position,
      lastOnChangedText,
      displayName
    )} `;

    setLastOnChangedText(result);
    handleChange(result);
  };

  return (
    <Container>
      <MentionList
        users={users}
        isVisible={isVisible}
        userSelected={handleSelectedUser}
      />
      <TextArea
        onChangeText={handleChange}
        onSelectionChange={handleSelectionChange}
        numberOfLines={4}
        textAlignVertical="top"
        autoFocus
        multiline>
        {value}
      </TextArea>
    </Container>
  );
}

MentionEditor.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
  onChange: PropTypes.func.isRequired,
  users: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      username: PropTypes.string,
      displayName: PropTypes.string,
    })
  ).isRequired,
  pivotKey: PropTypes.string,
};

MentionEditor.defaultProps = {
  pivotKey: 'username',
};
