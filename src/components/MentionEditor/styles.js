import styled from 'styled-components/native';

export const Container = styled.View``;

export const TextArea = styled.TextInput`
  width: 100%;
  height: auto;
  min-height: 200px;
  background: #fff;
  padding: 10px;
  border-radius: 4px;
  border: 1px solid #00000022;
  justify-content: center;
  font-size: 16px;
`;

export const Hightlight = styled.Text`
  background: #12312333;
  border-radius: 4px;
`;
