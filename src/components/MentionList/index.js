import React from 'react';
import PropTypes from 'prop-types';
import { Container } from './styles';
import Mention from '../Mention';

export default function MentionList({ users, isVisible, userSelected }) {
  return (
    <Container isVisible={isVisible}>
      {users.map(user => (
        <Mention key={user.id} user={user} onPress={userSelected} />
      ))}
    </Container>
  );
}

MentionList.propTypes = {
  users: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      username: PropTypes.string,
      displayName: PropTypes.string,
    })
  ).isRequired,
  isVisible: PropTypes.bool.isRequired,
  userSelected: PropTypes.func.isRequired,
};
