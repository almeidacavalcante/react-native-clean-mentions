import styled from 'styled-components/native';

export const Container = styled.View`
  display: ${props => (props.isVisible ? 'flex' : 'none')};
`;
