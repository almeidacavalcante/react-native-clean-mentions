import styled from 'styled-components/native';

export const Container = styled.View`
  display: flex;
  background-color: #7159c1;
  width: 100%;
  height: 100%;
  justify-content: space-around;
  padding: 10px;
`;
