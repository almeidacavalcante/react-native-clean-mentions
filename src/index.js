/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';
import { SafeAreaView } from 'react-native';
import MentionEditor from './components/MentionEditor';
import { Container } from './styles';

const App = () => {
  const [localValue, setLocalValue] = useState('');

  const handleChange = text => {
    setLocalValue(text);
  };

  const users = [
    {
      username: 'jairBolsonaro',
      displayName: 'Jair Bolsonaro',
      id: 'fuhdfusAsuh-Dhuashd-0asdas1',
    },
    {
      username: 'sergioMoro',
      displayName: 'Sérgio Moro',
      id: 'ASDasdsdasd-asdasd-0asdas2',
    },
    {
      username: 'pauloGuedes',
      displayName: 'Paulo Guedes',
      id: 'fddddfas-Dhuashd-Asdas',
    },
  ];

  const pivotKey = 'displayName';
  return (
    <>
      <SafeAreaView>
        <Container>
          <MentionEditor
            value={localValue}
            users={users}
            pivotKey={pivotKey}
            onChange={handleChange}
          />
        </Container>
      </SafeAreaView>
    </>
  );
};

export default App;
