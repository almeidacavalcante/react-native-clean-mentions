const getWordBoundsAt = (str, position) => {
  const isSpace = char => /\s/.exec(char);
  let start = position - 1;
  let end = position;

  while (start >= 0 && !isSpace(str[start])) {
    start -= 1;
  }
  start = Math.max(0, start + 1);

  while (end < str.length && !isSpace(str[end])) {
    end += 1;
  }
  end = Math.max(start, end);

  return [start, end];
};

const getFullWordAt = (positions, text) => {
  const [start, end] = positions;
  return text.substring(start, end);
};

const insertTextAt = (position, sourceText, insertion) => {
  const finalText =
    sourceText.substr(0, position - 1) +
    insertion +
    sourceText.substr(position, sourceText.length);

  return finalText;
};

/**
 * REGULAR EXPRESSIONS LIST
 */
// const rawMentionRegex = /@\[([^\]]+?)\]\(id:([^\]]+?)\)/g; // @[](id:)
// const rawMentionRegexFullMatch = /@\[[^\]]+?\]\(id:[^\]]+?\)/g;
// const mentionRegex = /(@[A-z]+)/g; // @name
// const extractUsernameRegex = /@\[([A-z]+)\]\(id:([^\]]+?)+\)/g;
// const mentionRegexFullMatch = /@[A-z ?]+#[0-9]+/g;

module.exports = {
  getWordBoundsAt,
  getFullWordAt,
  insertTextAt,
};
